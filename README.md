# clone-hostgator-ui

![layout_prices](https://github.com/lucas-sobierajski-oliveira/clone-hostgator-ui/blob/master/hostgator-prices-ui.png)

### iniciar o projeto
No diretório em que foi baixado o projeto executar o comando na raiz dele<br>
"yarn" ou "npm install"

executando o projeto com<br>
"yarn start"

Garanta que o servidor mysql esteja ativo<br>
Click nesse [link](https://github.com/lucas-sobierajski-oliveira/api) para acessar o repositório com a inicialização da api 

Acessando a aplicação<br>
http://localhost:3000/prices/1
