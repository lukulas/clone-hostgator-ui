import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import api from '~/services/api';
import { getProductSuccess } from '~/store/models/product/actions';

import Header from '~/components/Header';
import Outdoor from '~/components/Outdoor';
import Plan from '~/components/Plan';
import Background from '~/components/Background';

import { ContainerErro } from './styles';

function Main({ match }) {
  const dispatch = useDispatch();
  const { id } = match.params;
  const [currentProduct, setCurrentProduct] = useState();

  useEffect(() => {
    async function initProduct() {
      if (!currentProduct) {
        const response = await api.get(`products/${id}`);
        const product = Object.values(response.data.products)[0];
        dispatch(getProductSuccess(product));
        setCurrentProduct(product);
      }
    }

    initProduct();
  });

  return (
    <Background>
      <Header />
      <Outdoor />
      {currentProduct ? (
        <Plan />
      ) : (
        <ContainerErro>
          <h1>...Plano inexistente</h1>
        </ContainerErro>
      )}
    </Background>
  );
}

export default Main;
