import styled from 'styled-components';

export const ContainerErro = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;

  h1 {
    opacity: 0.6;
  }
`;
