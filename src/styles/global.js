import { createGlobalStyle } from 'styled-components';

export default createGlobalStyle`
@import url('https://fonts.googleapis.com/css2?family=Montserrat:wght@200;400&display=swap');

* {
  margin: 0;
  padding: 0;
  outline: 0;
  box-sizing: border-box;
}

html, body, #root {
  height: 100%;
}

body {
  font: 14px 'Montserrat';
  background: #fff;
  -webkit-font-smoothing: antialiased !important
}

ul {
  list-style: none;
}
`;
