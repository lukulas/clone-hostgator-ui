import produce from 'immer';

const INITIAL_STATE = {
  product: null,
  cycle: null,
};

export default function(state = INITIAL_STATE, action) {
  return produce(state, draft => {
    switch (action.type) {
      case '@product/GET_SUCCESS':
        draft.product = action.payload.product;
        break;
      case '@product/SET_CYCLE':
        draft.cycle = action.payload.cycle;
        break;
      default:
    }
  });
}
