import { all, call, takeLatest, put, } from 'redux-saga/effects';

import { getProductSuccess } from '~/store/models/product/actions';
import api from '~/services/api';

export function* getProduct({ payload }) {
  try {
    const response = yield call(api.get, `products/${payload.id}`);

    const product = Object.values(response.data.products)[0];

    yield put(getProductSuccess(product));
  } catch (err) {}
}

export default all([takeLatest('@product/GET_REQUEST', getProduct)]);
