export function getProductRequest(id) {
  return {
    type: '@product/GET_REQUEST',
    payload: {
      id,
    },
  };
}

export function getProductSuccess(product) {
  return {
    type: '@product/GET_SUCCESS',
    payload: {
      product,
    },
  };
}

export function setCycle(cycle) {
  return {
    type: '@product/SET_CYCLE',
    payload: {
      cycle,
    },
  };
}
