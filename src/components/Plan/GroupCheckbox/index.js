import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import { setCycle } from '~/store/models/product/actions';
import { Container, Radio } from './styles';

function GroupCheckbox() {
  const dispatch = useDispatch();
  const product = useSelector(state => state.product.product);
  const [currentCycle, setCurrentCycle] = useState();

  useEffect(() => {
    function initCycle() {
      if (!currentCycle) {
        const productCycle =
          product && Object.values(product.cycle).find(c => c.month === 36);
        setCurrentCycle(productCycle);
      }
    }

    initCycle();
  });

  useEffect(() => {
    dispatch(setCycle(currentCycle));
  }, [currentCycle, dispatch]);

  const [groupCheckBox, setGroupCheckBox] = useState([
    {
      id: 0,
      value: true,
      description: '3 anos',
      cycle: 36,
    },
    {
      id: 1,
      value: false,
      description: '1 ano',
      cycle: 12,
    },
    {
      id: 2,
      value: false,
      description: '1 mês',
      cycle: 1,
    },
  ]);

  function handleToggleCheck(id) {
    const group = groupCheckBox.map((_, i) => {
      if (i === id) {
        groupCheckBox[i].value = true;

        const productCycle = Object.values(product.cycle).find(
          c => c.month === groupCheckBox[i].cycle
        );
        setCurrentCycle(productCycle);
      } else {
        groupCheckBox[i].value = false;
      }
      return groupCheckBox[i];
    });

    setGroupCheckBox(group);
  }

  return (
    <Container>
      {groupCheckBox.map((el, _, i) => (
        <Radio selected={el.value} onClick={() => handleToggleCheck(el.id)}>
          <input type="radio" name="cycle" id={i} checked={el.value} />
          {el.description}
        </Radio>
      ))}
    </Container>
  );
}

export default GroupCheckbox;
