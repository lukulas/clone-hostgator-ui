import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  background: #fff;
  border-radius: 22px;
  margin-top: 6px;
  border: 2px solid #4480c5;
`;

export const Radio = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 8px 14px;
  border-radius: 22px;
  background: #fff;
  color: ${props => (props.selected ? '#fff' : '#4480c5')};
  background: ${props => (props.selected ? '#4480c5' : '#fff')};
  font-weight: ${props => (props.selected ? 'bold' : '')};
  cursor: pointer;

  input {
    margin: 0 2px;
    cursor: pointer;
  }
`;
