import React from 'react';

import GroupCheckbox from './GroupCheckbox';
import BoxPlan from './BoxPlan';
import { Container } from './styles';

function Plan() {
  return (
    <Container>
      <p>Quero pagar a cada:</p>
      <GroupCheckbox />
      <BoxPlan />
    </Container>
  );
}

export default Plan;
