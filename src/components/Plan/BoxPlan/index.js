import React, { useState, useEffect, useMemo } from 'react';
import { useSelector } from 'react-redux';
import history from '~/services/history';

import stackIcon from '~/assets/stack-icon.svg';
import {
  Container,
  ConfigPlanContainer,
  PriceContainer,
  TitleContainer,
} from './styles';

function BoxPlan() {
  const product = useSelector(state => state.product.product);
  const productCycle = useSelector(state => state.product.cycle);
  const [cycle, setCycle] = useState();

  const discount = useMemo(() => cycle && cycle.price_renew * 0.6, [cycle]);
  const plots = useMemo(
    () =>
      cycle &&
      ((cycle.price_renew * 0.6) / cycle.month).toLocaleString('pt-BR', {
        style: 'currency',
        currency: 'BRL',
      }),
    [cycle]
  );
  const discountValue = useMemo(
    () =>
      cycle &&
      (cycle.price_renew - cycle.price_renew * 0.6).toLocaleString('pt-BR', {
        style: 'currency',
        currency: 'BRL',
      }),
    [cycle]
  );

  useEffect(() => {
    setCycle(productCycle);
  }, [productCycle]);

  function handleOnClick(queryParams) {
    history.push(`/product?${queryParams}`);
  }

  return (
    <Container>
      <TitleContainer>
        <img src={stackIcon} alt="stack" />
        <h1>{product && product.name}</h1>
      </TitleContainer>
      <PriceContainer>
        <p>
          <s>R$ {cycle && cycle.price_renew}</s> <b>R$ {discount}</b>
        </p>
        <p>equivalente a</p>
        <p className="blue">
          R$ <span className="uppercase">{plots}/</span>mês*
        </p>
        <button
          type="button"
          onClick={() =>
            handleOnClick(
              `a=add&pid=${product.id}&billingcycle=${cycle.description}&promocode=PROMOHG40`
            )
          }
        >
          Contrate Agora
        </button>
        <p className="blue">
          <b>1 ano de Domínio Grátis</b>
        </p>
        <p className="blue space">
          economize R$ {discountValue} <span className="green">40% OFF</span>
        </p>
      </PriceContainer>
      <ConfigPlanContainer>
        <p>Sites ilimitados</p>
        <p>
          <b>100 GB</b> de Armazenamento
        </p>
        <p>
          Contas de E-mail <b>Ilimitadas</b>
        </p>
        <p>
          Criador de Sites <b>Grátis</b>
        </p>
        <p>
          Certificado SSL <b>Grátis</b>Grátis (https)
        </p>
      </ConfigPlanContainer>
    </Container>
  );
}

export default BoxPlan;
