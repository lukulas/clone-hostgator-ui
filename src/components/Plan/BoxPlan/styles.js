import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  background: #fff;
  margin: 40px;
  border-radius: 4px;
  padding: 20px;
  border-top: 10px solid #ff6a17;
  border-bottom: 4px solid #ff6a17;
  border-right: 1px solid #dfecff;
  border-left: 1px solid #dfecff;
`;

export const TitleContainer = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  border-bottom: 1px solid #dfecff;
  padding: 20px 0;

  h1 {
    margin-top: 6px;
    color: #1d5297;
    font-weight: bold;
  }
`;

export const PriceContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  border-bottom: 1px solid #dfecff;
  padding: 20px 0;

  button {
    cursor: pointer;
    font-size: 20px;
    background: #ff6a17;
    font-weight: bold;
    padding: 8px 40px;
    border: 0;
    border-radius: 20px;
    color: #fff;
    margin: 26px 0;
  }

  .space {
    line-height: 32px;
  }

  .blue {
    color: #1d5297;
  }

  p {
    .uppercase {
      font-size: 30px;
      font-weight: bold;
      color: #1d5297;
    }
    .green {
      background: #51c99c;
      font-weight: bold;
      color: #fff;
      padding: 4px 6px;
      border-radius: 14px;
    }
  }
`;

export const ConfigPlanContainer = styled.div`
  width: 100%;
  height: 100%;
  padding: 20px 0;

  p {
    line-height: 26px;
    border-bottom: 1px dashed #dfecff;
  }
`;
