import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 350px;
  margin-top: 130px;
  color: #fff;

  > img {
    width: 500px;
  }

  div {
    display: flex;
    height: 100%;
    flex-direction: column;
    align-items: center;
    width: 800px;

    h1 {
      text-align: center;
      padding: 2px 90px;
      font-size: 30px;
      font-weight: bold;
    }

    div {
      margin-top: 32px;
      img {
        margin-right: 6px;
      }
    }
  }
`;
