import React from 'react';

import officeImg from '~/assets/office.svg';
import manOfficeImg from '~/assets/man-office.svg';
import iconCheck from '~/assets/icon-check.svg';
import point from '~/assets/point.svg';
import { Container } from './styles';

function Outdoor() {
  return (
    <Container>
      <img src={officeImg} alt="office" />
      <div>
        <p>Hospedagem de Sites</p>
        <h1>
          Tenha uma hospedagem de sites estável e evite perder visitantes
          diariamente
        </h1>

        <div>
          <p>
            <img src={iconCheck} alt="check" />
            99,9% de disponibilidade: seu site sempre no ar
          </p>
          <p>
            <img src={iconCheck} alt="check" />
            Suporte 24h, todos os dias <img src={iconCheck} alt="check" />
            Painel de Controle cPanel
          </p>
        </div>

        <img src={point} alt="point" />
      </div>
      <img src={manOfficeImg} alt="manoffice" />
    </Container>
  );
}

export default Outdoor;
