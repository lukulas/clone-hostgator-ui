import React from 'react';

import logo from '~/assets/hostgator-logo.svg';
import { Container } from './styles';

function Header() {
  return (
    <Container>
      <div>
        <img src={logo} alt="" srcSet="" />
      </div>
    </Container>
  );
}

export default Header;
