import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 52px;
  background: #fff;

  div {
    width: 100%;
    display: flex;
    justify-content: left;
    max-width: 1100px;
    min-width: 320px;
  }

  img {
    padding: 0 8px;
  }
`;
